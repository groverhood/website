from flask import Flask, send_file

app = Flask(__name__, static_folder='static')

@app.route('/', methods=['GET'])
def index():
    return send_file('static/index.html')

# ===========
# === API ===
# ===========

@app.route('/api/projects', methods=['GET'])
def projects():
    pass
