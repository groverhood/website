const path = require('path');
module.exports = {
  mode: 'production',
  entry: path.join(__dirname, 'src', 'index.tsx'),
  output: {
    path: '/home/node/build',
    filename: "index.bundle.js"
  },
  module: {
    rules: [{
      test: /.tsx$/,
      include: [
        path.resolve(__dirname, 'src')
      ],
      exclude: [
        path.resolve(__dirname, 'node_modules')
      ],
      loader: 'babel-loader',
      query: {
        presets: ["@babel/typescript", "@babel/react", "@babel/env"]
      }
    }]
  }
};