import React from 'react'
import ReactDOM from 'react-dom'

function Root() {
    return <div id="RootComponent"><h1>Hello, world!</h1></div>
}

ReactDOM.render(<Root />, document.getElementById('root'))